# Prova Sicredi
Responsável: Isabel Cristina Dapper

Projeto criado como avaliação para vaga de teste de software.

**Estrutura**
* Java
* Maven
* Selenium
* JUnit

**Ambiente**
* Java [jdk8]
* NodeJS [última versão estável]
* WebDrive Manager
* IDE IntelliJ [Community]


**Desafio**

Passos (Desafio 01):
<!--suppress SpellCheckingInspection -->
<blockquote>
* Acessar a página https://www.grocerycrud.com/demo/bootstrap_theme;<br>
* Mudar o valor da combo Select version para “Bootstrap V4 Theme”;<br>
* Clicar no botão Add Customer;<br>
* Preencher os campos do formulário com as seguintes informações:

        Name: Teste Sicredi
        Last name: Teste
        ContactFirstName: seu nome
        Phone: 51 9999-9999
        AddressLine1: Av Assis Brasil, 3970
        AddressLine2: Torre D
        City: Porto Alegre
        State: RS
        PostalCode: 91000-000
        Country: Brasil
        from Employeer: Fixter
        CreditLimit: 200

* Clicar no botão Save;<br>
* Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção;<br>
* Fechar o browser.
</blockquote>

Passos (Desafio 02):
<blockquote>
* Execute todos os passos do Desafio 1;<br>
* Clicar no link Go back to list;<br>
* Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi);<br>
* Clicar no checkbox abaixo da palavra Actions;<br>
* Clicar no botão Delete;<br>
* Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada;<br>
* Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”;<br>
* Fechar o browser.
</blockquote>

##Boa Sorte!