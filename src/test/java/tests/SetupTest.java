package tests;

import io.qameta.allure.Story;
import org.junit.Test;
import pageObjects.AddCustomer;
import pageObjects.BootstrapTheme;
import pageObjects.BootstrapThemeV4;
import utils.Browser;
import utils.Utils;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class SetupTest extends BaseTests {

    @Test
    @Story("Challenge One")
    public void challengeOne() {

        //Open browser and load pages
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl()));
        BootstrapTheme bootstrapTheme = new BootstrapTheme();ajustes
        BootstrapThemeV4 bootstrapThemeV4 = new BootstrapThemeV4();
        AddCustomer addCustomer = new AddCustomer();

        //Change the value of the select version combo to “Bootstrap V4 Theme” (and check page)
        bootstrapTheme.selectVersion();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl()
            .concat("_v4")));

        //Click on Add Customer button (and check page)
        bootstrapThemeV4.addCustomer();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl()
            .concat("_v4/add")));

        //Fill the form fields with pre-defined information
        addCustomer.fillForm();

        //Click the Save button
        addCustomer.saveForm();

        //Validate the confirmation message with an assertion
        String message = addCustomer.getReport();
        String validReport = "Your data has been successfully stored into the database.";
        assertThat(message, containsString(validReport));

        //Close browser
    }

    @Test
    @Story("Challenge Two")
    public void challengeTwo(){

        //Load pages
        AddCustomer addCustomer = new AddCustomer();
        BootstrapThemeV4 bootstrapThemeV4 = new BootstrapThemeV4();

        //Perform all steps in Challenge One
        challengeOne();

        //Click the "Go back to list" link
        addCustomer.btnLink_goBack.click();

        //Click on the “Search Name” column and enter the contents of the Name (Teste Sicredi)
        bootstrapThemeV4.searchName();

        //Click on the checkbox below the word Actions
        bootstrapThemeV4.clickCheckbox();

        //Click the Delete button
        bootstrapThemeV4.clickDelete();

        //Validate the text through an assertion for the popup that will be presented
        assertEquals(bootstrapThemeV4.getAlertMultipleDelete(), "Are you sure that you want to delete those 10 items?");

        //Click the Delete button in the popup
        bootstrapThemeV4.clickDeletePopup();

        //Add an assertion to the green box message at the top right of the screen.
        //WebDriverWait wait = new WebDriverWait(driver, 10);
        //wait.until(ExpectedConditions.visibilityOf(bootstrapThemeV4.alert_successDelete));
        //assertEquals(bootstrapThemeV4.getAlertSuccessDelete(), "Your data has been successfully deleted from the database.");

        //Close

    }
}



