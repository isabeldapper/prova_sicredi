package pageObjects;

import elementMapper.AddCustomerElementMapper;
import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class AddCustomer extends AddCustomerElementMapper {

    public AddCustomer(){
        PageFactory.initElements(Browser.getCurrentDriver(), this);
    }

    public void fillForm(){
        first_name.sendKeys("Teste Sicredi");
        last_name.sendKeys("Teste");
        contact_name.sendKeys("Isabel");
        phone_number.sendKeys("51 9999-9999");
        address_line1.sendKeys("Av Assis Brasil, 3970");
        address_line2.sendKeys("Torre D");
        city_name.sendKeys("Porto Alegre");
        state_name.sendKeys("RS");
        postal_code.sendKeys("91000-000");
        country_name.sendKeys("Brasil");
        credit_limit.sendKeys("200");
        select_employeerOptions.click();
        select_Fixter.click();

    }

    public void saveForm(){
        btn_save.click();
    }

    public String getReport(){
        return report_success.getText();
    }

    public void backToList(){
        btnLink_goBack.click();
    }

}
