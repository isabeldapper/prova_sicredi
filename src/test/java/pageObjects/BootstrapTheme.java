package pageObjects;

import elementMapper.BootstrapThemeElementMapper;
import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class BootstrapTheme extends BootstrapThemeElementMapper {

    public BootstrapTheme(){
        PageFactory.initElements(Browser.getCurrentDriver(), this);
    }

    public void selectVersion(){
        select_version.click();
    }
}
