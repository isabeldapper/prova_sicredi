package pageObjects;

import elementMapper.BootstrapThemeV4ElementMapper;
import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class BootstrapThemeV4 extends BootstrapThemeV4ElementMapper {

    public BootstrapThemeV4(){
        PageFactory.initElements(Browser.getCurrentDriver(), this);
    }

    public void addCustomer(){
        btn_addCustomer.click();
    }

    public void searchName() {
        search_name.sendKeys("Teste Sicredi");
    }

    public void clickCheckbox() {
        btn_checkbox.click();
    }

    public void clickDelete() {
        btn_delete.click();
    }

    public String getAlertMultipleDelete() {
        return alert_multipleDelete.getText();
    }

    public void clickDeletePopup(){
        btn_deletePopup.click();
    }

    public String getAlertSuccessDelete(){
        return alert_successDelete.getText();
    }
}
