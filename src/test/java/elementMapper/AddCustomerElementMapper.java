package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddCustomerElementMapper {

    @FindBy(id = "field-customerName")
    public WebElement first_name;

    @FindBy(id = "field-contactLastName")
    public WebElement last_name;

    @FindBy(id = "field-contactFirstName")
    public WebElement contact_name;

    @FindBy(id = "field-phone")
    public WebElement phone_number;

    @FindBy(id = "field-addressLine1")
    public WebElement address_line1;

    @FindBy(id = "field-addressLine2")
    public WebElement address_line2;

    @FindBy(id = "field-city")
    public WebElement city_name;

    @FindBy(id = "field-state")
    public WebElement state_name;

    @FindBy(id = "field-postalCode")
    public WebElement postal_code;

    @FindBy(id = "field-country")
    public WebElement country_name;

    @FindBy(id = "field_salesRepEmployeeNumber_chosen")
    public WebElement select_employeerOptions;

    @FindBy(css = ".chosen-results .active-result[data-option-array-index = \"8\"]")
    public WebElement select_Fixter;

    @FindBy(id = "field-creditLimit")
    public WebElement credit_limit;

    @FindBy(id = "form-button-save")
    public WebElement btn_save;

    @FindBy(css = "#report-success > p")
    public WebElement report_success;

    @FindBy(css = "#report-success > p > a:nth-child(2)")
    public WebElement btnLink_goBack;
}
