package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BootstrapThemeV4ElementMapper {

    @FindBy(css = "#gcrud-search-form > div.header-tools > div.floatL.t5 > a")
    public WebElement btn_addCustomer;

    @FindBy(css = "#gcrud-search-form > div.scroll-if-required > table > thead > tr.filter-row.gc-search-row > td:nth-child(3) > input")
    public WebElement search_name;

    @FindBy(className = "select-all-none")
    public WebElement btn_checkbox;

    @FindBy(css = "#gcrud-search-form > div.scroll-if-required > table > thead > tr.filter-row.gc-search-row > td.no-border-left > div.floatL > a > i")
    public WebElement btn_delete;

    @FindBy(css = "body > div.container-fluid.gc-container > div.row > div.delete-multiple-confirmation.modal.fade.in.show > div > div > div.modal-body > p.alert-delete-multiple")
    public WebElement alert_multipleDelete;

    @FindBy(css = "body > div.container-fluid.gc-container > div.row > div.delete-multiple-confirmation.modal.fade.in.show > div > div > div.modal-footer > button.btn.btn-danger.delete-multiple-confirmation-button")
    public WebElement btn_deletePopup;

    @FindBy(css = "body > div.alert.alert-success.growl-animated.animated.bounceInDown > span:nth-child(4) > p")
    public WebElement alert_successDelete;
}
