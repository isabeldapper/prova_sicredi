package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BootstrapThemeElementMapper {

    @FindBy(css = "#switch-version-select > option:nth-child(2)")
    public WebElement select_version;

}
